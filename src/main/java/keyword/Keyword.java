package keyword;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static object.repository.ObjectRepository.*;

public class Keyword {

    private static WebDriver driver;

    public static void ouvrirNavigateur(String website) {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get(website);
        driver.switchTo().defaultContent();
    }

    public static void fermerNavigateur() {
        driver.quit();
    }

    public static void saisirCaracteristiquesVehicule(Object brand, Object model, Object fuel) {
        select(carBrand, brand.toString());
        select(carModel, model.toString());
        select(fuelType, fuel.toString());
    }

    public static void saisirAnneeMiseEnCirculation(Object year) {
        if(year.toString().equals("NA")){
            Assert.fail("Release year is null");
        }
        else{
            fill(firstUseYear, year);
        }
    }

    public static void saisirLesDonneesConducteur(Object licenseDateText, Object zipCodeText, Object nameText) {
        fill(name, nameText.toString());
        fill(zipCode, zipCodeText.toString());
        fill(licenseDate, licenseDateText.toString());
    }

    public static void saisirBonusMalus(Object bonusMalusText) {
        fill(bonusMalus, bonusMalusText.toString());
    }

    public static void choisirAssuranceAuTiers() {
        clickOn(thirdParty);
    }

    public static void choisirAssuranceTousRisques() {
        clickOn(comprehensive);
    }

    public static void simulerEtVerifierTarif(String pricing) {
        clickOn(simulateButton);
        checkMessage(fee, pricing.toString());
    }

    public static void verifierMessageErreur() {
        checkMessage(carError, "Véhicule non trouvé");
    }

    // UTILITY METHODS

    /**
     * click on web element
     *
     * @param xpath is element to click on
     */
    public static void clickOn(String xpath) {
        driver.findElement(By.xpath(xpath)).click();
    }

    /**
     * waits an amount of time
     *
     * @param time in seconds
     */
    public static void wait(int time) {
        driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
    }

    /**
     * check if text of given web element is equals to expected text
     *
     * @param xpath web element
     * @param text  expected text
     */
    public static void checkMessage(String xpath, String text)  {
        WebElement element = waitForWebElement(xpath);
        if (element == null)
        {
            Assert.fail("Element not found: " + xpath);
        }
        else
        {
            if (element.getText().equals(text))
            {
                Assert.assertTrue(true);
            }
            else
            {
                Assert.fail("Incorrect message: " + element.getText() + "\nExpected: " + text);
            }
        }
    }

    /**
     * selects option of select element
     *
     * @param xpath element
     * @param text  text of option
     */
    public static void select(String xpath, Object text) {
        Select select = new Select(driver.findElement(By.xpath(xpath)));
        select.selectByValue(text.toString());
    }

    /**
     * fills input or date
     *
     * @param xpath element
     * @param text  text to type
     */
    public static void fill(String xpath, Object text) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.clear();
        element.sendKeys(text.toString());
    }

    /**
     * check if element if displayed on screen
     *
     * @param xpath element
     */
    public static void checkElementIsPresent(String xpath) {
        if (driver.findElement(By.xpath(xpath)).isDisplayed()) {
            Assert.assertTrue(true);
        } else {
            Assert.fail("Element is not present: " + xpath);
        }
    }

    /**
     * wait for element to be displayed on screen
     *
     * @param xpath element
     */
    public static WebElement waitForWebElement(String xpath) {
        WebDriverWait wait = new WebDriverWait(driver, 3);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

        if(element == null){
            Assert.fail("Element not found: " + xpath);
        }

        return element;
    }

}
