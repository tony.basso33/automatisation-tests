package object.repository;

public class ObjectRepository {

    public static String carBrand = "//*[@id='carBrand']";
    public static String carModel = "//*[@id='carModel']";
    public static String fuelType = "//*[@id='fuelType']";
    public static String firstUseYear = "//*[@id='firstUseYear']";
    public static String checkboxIsNewCar = "//*[@id='isNewCar']";

    public static String name = "//*[@id='name']";
    public static String licenseDate = "//*[@id='licenseDate']";
    public static String zipCode = "//*[@id='zipCode']";
    public static String bonusMalus = "//*[@id='bonusMalus']";
    public static String comprehensive = "//*[@id='comprehensive']";
    public static String thirdParty = "//*[@id='thirdParty']";

    public static String simulateButton = "//*[@id='simulateButton']";
    public static String fee = "//*[@id='fee']";
    public static String errorMessage = "//*[@id='errorMessage']";
    public static String carError = "//*[@id='carError']";


    public static void buildObjectRepository(){ }

}
